<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'post_id',
        'post_title',
        'post_content',
        'post_image',
        'post_status',
        'user_id',
        'category_id',

    ];

    public function Categories()
    {
        return $this->hasMany(Category::class);
        //return $this->hasOne(Category::className(), ['category_id' => 'id']);
    }


    public function deliver()
    {
        $result = '';
        if (($this->cons_amphoe_id != null) && ($this->cons_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->cons_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }
}
