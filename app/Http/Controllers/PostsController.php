<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Posts;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\Input;

class PostsController extends Controller
{

    public function home()
    {
        $posts = DB::table('posts')
            ->leftjoin('categories', 'posts.category_id', '=', 'categories.id')
            ->select('posts.*', 'categories.*')
            ->where('post_status', '=', 1)
            ->get();
        return view('posts', compact('posts', $posts));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::all();

        return view('posts/index', compact('posts', $posts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'post_title' => 'required',
            'post_content' => 'required',
            'post_status' => 'required',
            'post_image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',
            'user_id' => 'required',
            'category_id' => 'required',

        ]);

        $stringImaageReFormat = base64_encode('_' . time());
        $ext = $request->file('post_image')->getClientOriginalExtension();
        $imageName = $stringImaageReFormat . "." . $ext;
        $imageEncoded = File::get($request->post_image);

        Storage::disk('local')->put('public/images/' . $imageName, $imageEncoded);

        $posts = new Posts();
        $posts->post_title = $request->post_title;
        $posts->post_content = $request->post_content;
        $posts->post_status = $request->post_status;
        $posts->post_image = $imageName;
        $posts->user_id = $request->user_id;
        $posts->category_id = $request->category_id;
        $posts->save();
        Session()->flash("success", "บันทึกข้อมูลเรียบร้อย!! --> $posts->post_title");
        return redirect('posts/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function view(Posts $posts)
    {
        $postsc = DB::table('posts')
            ->leftjoin('categories', 'posts.category_id', '=', 'categories.id')
            ->select('posts.*', 'categories.*')->get();


        return view('posts.view', [
            'posts' => $posts,
            'postsc' => $postsc,

        ])
            ->with('category', Category::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $posts)
    {
        //dd($posts);
        return view('posts.edit', [
            'posts' => $posts
        ]);
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $posts)
    {
        //

        if ($request->hasFile('post_image')) {
            $stringImaageReFormat = base64_encode('_' . time());
            $ext = $request->file('post_image')->getClientOriginalExtension();
            $imageName = $stringImaageReFormat . "." . $ext;
            $imageEncoded = File::get($request->post_image);
            $oldimage = $posts->post_image;
            $posts->post_image = $ext;
            Storage::disk('local')->delete('public/images/' . $oldimage);
            Storage::disk('local')->put('public/images/' . $imageName, $imageEncoded);
            try {
                $image_new = [
                    'post_image' => $imageName
                ];
                $posts->update($image_new);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }

        //Storage::disk('local')->delete('public/images/' . $imageName, $imageEncoded);



        try {
            $datas = [
                'post_title' => $request->post_title,
                'post_content' => $request->post_content,
                'post_status' => $request->post_status,
                //'post_image' => $imageName,
                'user_id' => $request->user_id,
                'category_id' => $request->category_id,

            ];
            $posts->update($datas);
        } catch (Exception $e) {
            dd($e->getMessage());
        }
        Session()->flash("success", "แก้ไขข้อมูลเรียบร้อย --> $posts->post_title");

        return redirect()->route('posts.view', $posts->post_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy($posts)
    {

        $post = Posts::find($posts);
        $post::destroy($posts);
        Storage::disk('local')->delete('public/images/' . $post->post_image);
        Session()->flash("success", "ลบข้อมูลเรียบร้อย --> $post->post_title");
        return redirect('posts/index');
    }
}
