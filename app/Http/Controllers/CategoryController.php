<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories/index', compact('categories', $categories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
            'category_status' => 'required'
        ]);

        $categories = new Category();
        $categories->category_name = $request->category_name;
        $categories->category_status = $request->category_status;
        Session()->flash("success", "เพิ่มข้อมูลเรียบร้อย --> $categories->category_name");
        $categories->save();
        return redirect('categories/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::find($category);
        //dd($categories);
        return view('categories/edit', compact('category', $category));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        $request->validate([
            'category_name' => 'required',
            'category_status' => 'required'
        ]);

        $categories = Category::find($category);

        $categories->category_name = $request->category_name;
        $categories->category_status = $request->category_status;
        //dd($categories->category_name);
        Session()->flash("success", "แก้ไขข้อมูลเรียบร้อย --> $categories->category_name");
        $categories->save();
        return redirect('categories/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        $categories = Category::find($category);
        $categories::destroy($category);
        Session()->flash("success", "ลบข้อมูลเรียบร้อย --> $categories->category_name");
        return redirect('categories/index');
    }
}
