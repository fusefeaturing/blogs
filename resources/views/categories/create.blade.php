@extends('layouts.main')

@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="table-responsive">
    <h2>ชื่อประเภทบล๊อก</h2>
    <form action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อประเภทบล๊อก</label>
            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="ชื่อประเภทบล๊อก">
        </div>

        <div class="form-group">
            <label for="name">สถานะ</label>
            <input type="text" class="form-control" name="category_status" id="category_status" placeholder="สถานะ">
        </div>

        <button type="submit" name="submit" class="btn btn-success">เพื่ม</button>
    </form>
</div>

@endsection