@extends('layouts.main')

@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <h1>บล๊อก</h1>


    <a href="{{route('categories.create')}}" class="btn btn-success"> + เพิ่มประเภทบล๊อก</a>


    <div class="table-responsive my-2">
        <table class="table">
            <thead class="thead-dark">
                <tr>

                    <th scope="col">category name</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td><a href="/categories/{{$category->category_name}}"></a>{{$category->category_name}}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('categories.edit',['category'=>$category->id])}}">
                                <button type="button" class="btn btn-warning">Edit</button>
                            </a>
                        </div>
                    </td>

                    <td>
                        <a href="{{route('categories.destroy',['category'=>$category->id])}}" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่ ?')" class="btn btn-danger">Detele</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>


</div>

@endsection