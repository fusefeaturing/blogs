@extends('layouts.main')
@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<div class="table-responsive">
    <h2>แก้ไข : {{$category->category_name}}</h2>
    <form action="{{route('categories.update',['category'=>$category->id])}}" method="post">

        {{csrf_field()}}

        <div class="form-group">
            <label for="name">ชื่อประเภท</label>
            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="ชื่อประเภท" value="{{$category->category_name}}">
        </div>

        <div class="form-group">
            <label for="name">สถานะ</label>
            <input type="text" class="form-control" name="category_status" id="category_status" placeholder="สถานะ" value="{{$category->category_status}}">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">แก้ไข</button>
    </form>
</div>

@endsection