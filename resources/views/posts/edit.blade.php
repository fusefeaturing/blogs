@extends('layouts.main')
@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="table-responsive">
<h2>แก้ไข : {{$posts->post_title}}</h2>
    <form action="{{route('posts.update',['posts'=>$posts->post_id])}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อบล๊อก</label>
        <input type="text" class="form-control" name="post_title" id="post_title" value="{{$posts->post_title}}">
        </div>
        
        <div class="form-group">
            <label for="name">รูปภาพ</label>  <br>
            <img src="{{asset('storage')}}/images/{{$posts->post_image}}" alt="" width="100px" height="100px" style="margin: 10px;" >
            <input type="file" name="post_image" id="post_image" class="form-control" onchange="previewImg(this)">
            <img id="previewImage" style="max-width: 130px; margin-top:20px;" > 
        </div>
        <div class="form-group">
            <label for="name">เนื้อหา</label>            
            {{-- <input type="text" class="form-control" name="post_content" id="post_content" placeholder="สถานะ" value="{{$posts->post_content}}"> --}}
            <textarea name="post_content" id="post_content" class="form-control" placeholder="เนื้อหา" cols="30" rows="10">{{$posts->post_content}}</textarea>                
        </div>

        <div class="form-group">
            <label for="name">สถานะ</label>
            <input type="text" class="form-control" name="post_status" id="post_status" placeholder="สถานะ" value="{{$posts->post_status}}">
        </div>

        <div class="form-group">
            {{-- <label for="name">ผู้ใช้งาน : {!! Auth::user()->name !!}</label> --}}
            <input type="hidden" class="form-control" name="user_id" id="user_id" placeholder="ผู้ใช้งาน" value="1">                
        </div>

        <div class="form-group">
            <label for="name">ประเภทบล๊อก</label>
            <?php
                $categories = DB::table('categories')->select('*')->get();
            ?>
            <select class="form-control" name="category_id">
                <option value="">เลือกประเภทบล๊อก</option>
                @foreach($categories as $category)
            <option value="{{$category->id}}"
                @if($category->id == $category->id)
                selected
                @endif
            >
                {{$category->category_name}}</option>
                    @endforeach

                </select>                
        </div>

        <button type="submit" name="submit" class="btn btn-primary">UPDATE</button>
    </form>
</div>


@endsection