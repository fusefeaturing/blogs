@extends('layouts.main')

@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <h1>บล๊อก</h1>


    <a href="{{route('posts.create')}}" class="btn btn-success"> + เพิ่มบล๊อก</a>


    <div class="table-responsive my-2">
        <table class="table">
            <thead class="thead-dark">
                <tr>

                    <th scope="col">post title</th>
                    <th scope="col">View</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                    <td><a href="/posts/{{$post->post_title}}"></a>{{$post->post_title}}</td>
                    
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('posts.view',['posts'=>$post->post_id])}}">
                                <button type="button" class="btn btn-success">View</button>
                            </a>
                        </div>
                    </td>
                    
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('posts.edit',['posts'=>$post->post_id])}}">
                                <button type="button" class="btn btn-warning">Edit</button>
                            </a>
                        </div>
                    </td>

                    <td>
                        <a href="{{route('posts.destroy',['posts'=>$post->post_id])}}" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่ ?')" class="btn btn-danger">Detele</a>
                     
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>


</div>

@endsection