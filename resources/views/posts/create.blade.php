@extends('layouts.main')



@section('body')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="table-responsive">
        <h2>หัวเรื่อง</h2>
        <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">ชื่อบล๊อก</label>
                <input type="text" class="form-control" name="post_title" id="post_title" placeholder="ชื่อประเภทบล๊อก">
            </div>
            <div class="form-group">
                <label for="name">รูปภาพ</label>
                <input type="file" name="post_image" id="post_image" class="form-control" placeholder="รูปภาพ" onchange="previewImg(this)">  
                <img id="previewImage" alt="" style="max-width: 130px; margin-top:20px;">              
            </div>
            
            <div class="form-group">
                <label for="name">เนื้อหา</label>
                <textarea name="post_content" id="post_content" class="form-control" placeholder="เนื้อหา" cols="30" rows="10"></textarea>                
            </div>
            
            <div class="form-group">
                <label for="name">สถานะ</label>
                <input type="text" class="form-control" name="post_status" id="post_status" placeholder="สถานะ" value="1">
            </div>

            <div class="form-group">
                {{-- <label for="name">ผู้ใช้งาน : {!! Auth::user()->name !!}</label> --}}
                <input type="hidden" class="form-control" name="user_id" id="user_id" placeholder="ผู้ใช้งาน" value="1">                
            </div>

            <div class="form-group">
                <label for="name">ประเภทบล๊อก</label>
                <?php
                    $categories = DB::table('categories')->select('*')->get();
                ?>
                <select class="form-control" name="category_id">
                    <option value="">เลือกประเภทบล๊อก</option>
                            @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                    </select>                
            </div>
    
            <button type="submit" name="submit" class="btn btn-primary">ADD</button>
        </form>
    </div>



@endsection