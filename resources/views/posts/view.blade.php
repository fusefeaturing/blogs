@extends('layouts.main')

@section('body')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

        <div class="container">
            <div class="row">
                <div class=""></div>
                <div class="form-group">
                    <a href="{{route('posts.create')}}" class="btn btn-success"> + Add post</a>
                    <a href="{{route('posts.edit',['posts'=>$posts->post_id])}}">
                        <button type="button" class="btn btn-warning">Edit</button>
                    </a>
                    <a href="{{route('posts.destroy',['posts'=>$posts->post_id])}}" 
                        onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่ ?')" class="btn btn-danger">Detele</a>
                </div>
            </div>

    
        {{csrf_field()}}
        <div class="form-group">
            <label for="name"><h2>ชื่อบล๊อก : {{$posts->post_title}}</h2></label>
            
       
        </div>
        
        <div class="form-group">
            <label for="name">รูปภาพ</label>  <br>
            <img src="{{asset('storage')}}/images/{{$posts->post_image}}" alt="" width="100px" height="100px" style="margin: 10px;">
            
        </div>
        <div class="form-group">
            {{-- <label for="name">เนื้อหา : {{$posts->post_content}}</label>  --}}
             <label for="name" >เนื้อหา : {!!$posts->post_content!!}</label>
            
        </div>
        <?php
if ($posts->post_status == '1'){
    $status = "พร้อมใช้งาน"; 
} else {
    $status = "ไม่พร้อมใช้งาน"; 
}
    
?>
        <div class="form-group">
            <label for="name">สถานะ :  {{$status}} </label>
            
        </div>

        <div class="form-group">
            <label for="name">ผู้ใช้งาน : {!! Auth::user()->name !!}</label> 
            <input type="hidden" class="form-control" name="user_id" id="user_id" placeholder="ผู้ใช้งาน" value="1">                
        </div>

        <div class="form-group">

            <?php

                $categories = DB::table('categories')->select('*')->get();
                $post = DB::table('posts')
            ->leftjoin('categories', 'posts.category_id', '=', 'categories.id')
            ->select('posts.*', 'categories.category_name')->get();

//dd($posts->category_id);
            ?>
            
         <label for="name">ประเภทบล๊อก : @foreach($postsc as $post) 
            {{$post->category_name}}
                @endforeach 
        </label> 
        
            {{-- <select class="form-control" name="category_id">
                <option value="">เลือกประเภทบล๊อก</option>
                @foreach($postsc as $post)
            <option value="{{$post->id}}"
                @if($post->id == $post->id)
                selected
                @endif
            >
                {{$post->category_name}}</option>
                    @endforeach

                </select>                 --}}
        </div>

        
        </div>

@endsection