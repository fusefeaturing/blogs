<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('posts');
// });

Route::get('/', [PostsController::class, 'home'])->name('posts.home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Posts
Route::get('/posts/index', [PostsController::class, 'index'])->name('posts.index');
Route::get('/posts/create', [PostsController::class, 'create'])->name('posts.create');
Route::post('/posts/store', [PostsController::class, 'store'])->name('posts.store');
Route::get('/posts/edit/{posts}', [PostsController::class, 'edit'])->name('posts.edit');
Route::get('/posts/view/{posts}', [PostsController::class, 'view'])->name('posts.view');
Route::post('/posts/update/{posts}', [PostsController::class, 'update'])->name('posts.update');
Route::get('/posts/destroy/{posts}', [PostsController::class, 'destroy'])->name('posts.destroy');
//Route::get('/storage/app/public/images/{filename}', [PostsController::class, 'image'])->name('posts.image');


//Categories
Route::get('/categories/index', [CategoryController::class, 'index'])->name('categories.index');
Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
Route::post('/categories/store', [CategoryController::class, 'store'])->name('categories.store');
Route::get('/categories/edit/{category}', [CategoryController::class, 'edit'])->name('categories.edit');
Route::post('/categories/update/{category}', [CategoryController::class, 'update'])->name('categories.update');
Route::get('/categories/destroy/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');

//Route::get('/posts/index', [App\Http\Controllers\PostsController::class, 'index']);
